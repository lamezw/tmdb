<?php

use yii\db\Migration;

class m160127_200000_create_movies_table extends Migration
{

	public function up()
	{
		$this->createTable('{{%movie}}', [
			'id'			 => $this->primaryKey(),
			'title'			 => $this->string(255),
			'original_title' => $this->string(255),
			'release_date'	 => $this->date(),
			'runtime'		 => $this->integer(),
			'overview'		 => $this->text(),
			'poster_path'	 => $this->string(255),
		]);
	}

	public function down()
	{
		$this->dropTable('{{%movie}}');
	}

}
