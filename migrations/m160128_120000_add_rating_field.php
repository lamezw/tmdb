<?php

use yii\db\Migration;

class m160128_120000_add_rating_field extends Migration
{

	public function up()
	{
		$this->addColumn('{{%movie}}', 'rating', $this->decimal(2));
	}

	public function down()
	{
		if ($this->db->driverName === 'sqllite') {
			$this->dropColumn('{{%movie}}', 'rating');
		}
	}

}
