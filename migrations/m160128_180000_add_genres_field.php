<?php

use yii\db\Migration;

class m160128_180000_add_genres_field extends Migration
{

	public function up()
	{
		$this->addColumn('{{%movie}}', 'genres', $this->string(255));
	}

	public function down()
	{
		if ($this->db->driverName === 'sqllite') {
			$this->dropColumn('{{%movie}}', 'genres');
		}
	}

}
