<?php

namespace app\models;

use Yii;

class User extends \yii\base\Object implements \yii\web\IdentityInterface
{

	public $apiKey;
	public $sessionId;

	/**
	 * @inheritdoc
	 */
	public static function findIdentity($id)
	{
		return new static(['apiKey' => $id, 'sessionId' => Yii::$app->session->get('sessionId')]);
	}

	/**
	 * @inheritdoc
	 */
	public static function findIdentityByAccessToken($token, $type = null)
	{
		return null;
	}

	/**
	 * @inheritdoc
	 */
	public function getId()
	{
		return $this->apiKey;
	}

	/**
	 * @inheritdoc
	 */
	public function getAuthKey()
	{
		return false;
	}

	/**
	 * @inheritdoc
	 */
	public function validateAuthKey($authKey)
	{
		return false;
	}

}
