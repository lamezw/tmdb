<?php

namespace app\models;

use Yii;
use yii\base\Model;

/**
 * LoginForm is the model behind the login form.
 */
class LoginForm extends Model
{

	public $apiKey;
	public $sessionId;

	/**
	 * @return array the validation rules.
	 */
	public function rules()
	{
		return [
			[
				'apiKey',
				'required',
			],
			[
				'apiKey',
				'getSessionId',
			],
		];
	}

	public function getSessionId($attribute, $params)
	{
		$tmdbService	 = new \app\helpers\TMDBService($this->{$attribute});
		$this->sessionId = $tmdbService->getSessionId();

		if (!$this->sessionId) {
			$this->addError($attribute, $tmdbService->getFirstError('apiKey'));
		}
	}

	public function login()
	{
		if ($this->validate()) {
			$user = new \app\models\User(['apiKey' => $this->apiKey]);

			if (Yii::$app->user->login($user, 3600)) {
				Yii::$app->session->set('sessionId', $this->sessionId);
				return true;
			}
		}
		return false;
	}

}
