<?php

namespace app\models;

use \app\helpers\TMDBService;

class Movie extends \yii\db\ActiveRecord
{

	public function rules()
	{
		return [
			[
				[
					'id',
					'title',
					'original_title',
					'release_date',
					'runtime',
					'overview',
					'poster_path',
					'rating',
					'genres',
				],
				'safe',
			],
		];
	}

	protected static $requestUri = '/movie/';

	/**
	 * @var TMDBService
	 */
	protected $TMDBService;

	public static function findOne($condition, $TMDBService = null)
	{
		$model = parent::findOne($condition);
		if ($model === null && is_numeric($condition) && $TMDBService !== null) {
			$model = self::findOnTMDB($condition, $TMDBService);
		}

		$model->TMDBService = $TMDBService;

		return $model;
	}

	public static function findOnTMDB($id, TMDBService $TMDBService)
	{
		$movieData = $TMDBService->query(self::$requestUri . intval($id));

		if ($movieData['id']) {
			$genres = [];
			foreach ($movieData['genres'] as $genre) {
				$genres[] = $genre['name'];
			}
			$movieData['genres'] = implode(', ', $genres);

			$model = new static(array_intersect_key($movieData, self::getTableSchema()->columns));

			if ($model->poster_path) {
				$request	 = curl_init('https://image.tmdb.org/t/p/w185' . $model->poster_path);
				$savedImage	 = fopen(\Yii::getAlias('@webroot') . '/images' . $model->poster_path, 'wb');
				curl_setopt($request, CURLOPT_FILE, $savedImage);
				curl_setopt($request, CURLOPT_HEADER, 0);
				curl_exec($request);
				curl_close($request);
				fclose($savedImage);

				$model->poster_path = '/images' . $model->poster_path;
			}

			$model->save();

			return $model;
		}
	}

	public function beforeSave($insert)
	{
		if (!$insert && $this->oldAttributes['rating'] != $this->rating && $this->TMDBService !== null) {
			if (null === $this->TMDBService->query(self::$requestUri . intval($this->id) . '/rating', json_encode(['value' => floatval($this->rating)]))) {
				$this->addError('rating', $this->TMDBService->getFirstError('error'));
				return false;
			}
		}

		return parent::beforeSave($insert);
	}

	public function beforeDelete()
	{
		if ($this->poster_path && file_exists(\Yii::getAlias('@webroot') . $this->poster_path)) {
			unlink(\Yii::getAlias('@webroot') . $this->poster_path);
		}

		parent::beforeDelete();
	}

}
