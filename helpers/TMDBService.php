<?php

namespace app\helpers;

class TMDBService extends \yii\base\Model
{

	protected $requestUri = 'http://api.themoviedb.org/3';
	protected $apiKey;
	protected $sessionId;

	public function __construct($apiKey, $sessionId = false)
	{
		$this->apiKey	 = $apiKey;
		$this->sessionId = $sessionId;
	}

	public function getSessionId()
	{
		$resultData = $this->query("/authentication/guest_session/new");

		if (!$this->hasErrors()) {
			return $this->sessionId = $resultData['guest_session_id'];
		} else {
			$this->addError('apiKey', $this->getFirstError('error'));
		}
	}

	public function query($uri, $postBody = "")
	{
		if (strpos($uri, '?') === false) {
			$uri .= '?';
		} else {
			$uri .= '&';
		}

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
		curl_setopt($ch, CURLOPT_HEADER, FALSE);

		if ($postBody) {
			$uri .= "guest_session_id={$this->sessionId}&";

			curl_setopt($ch, CURLOPT_POST, TRUE);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postBody);
		}

		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			"Accept: application/json",
			"Content-Type: application/json",
		]);

		curl_setopt($ch, CURLOPT_URL, "{$this->requestUri}{$uri}api_key={$this->apiKey}");
		$response = json_decode(curl_exec($ch), true);

		if (curl_getinfo($ch, CURLINFO_HTTP_CODE) >= 300) {
			$this->addError('error', $response['status_message']);
		}

		curl_close($ch);

		if (!$this->hasErrors()) {
			return $response;
		}
	}

}
