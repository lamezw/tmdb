<?php

namespace app\helpers;

class TMDBMoviesProvider extends \yii\data\BaseDataProvider
{

	private $_lastResult;
	protected $requestUri = '/discover/movie';
	protected $TMDBService;
	protected $type;

	public function __construct(TMDBService $TMDBService, $type = null, $config = [])
	{
		$this->TMDBService	 = $TMDBService;
		$this->type			 = $type;

		parent::__construct($config);
	}

	public function prepare($forcePrepare = false)
	{
		if (empty($this->_lastResult) || $forcePrepare) {

			switch ($this->type) {
				case 'screen':
					$uri = "{$this->requestUri}?sort_by=release_date.desc&release_date.gte=" . date('Y-m-d', time() - 86400 * 60) . "&release_date.lte=" . date('Y-m-d');
					break;
				default:
					$uri = "{$this->requestUri}?sort_by=popularity.desc";
			}

			$this->setPagination([
				'pageSize'		 => 20,
				'validatePage'	 => false,
			]);

			if (($pagination = $this->getPagination()) !== false) {
				$uri .= '&page=' . ($pagination->getPage(true) + 1);
			}

			/*
			  if (($sort = $this->getSort()) !== false) {
			  $query->addOrderBy($sort->getOrders());
			  }
			 */

			$this->_lastResult = $this->TMDBService->query($uri);
			$this->setPagination([
				'pageSize'		 => 20,
				'validatePage'	 => false,
				'totalCount'	 => $this->prepareTotalCount(),
			]);
		}

		parent::prepare($forcePrepare);
	}

	protected function prepareModels()
	{
		$models = [];
		foreach ($this->_lastResult['results'] as &$movieData) {
			$models[] = new TMDBMovie(array_intersect_key($movieData, ['id' => true, 'title' => true, 'release_date' => true]));
		}

		return $models;
	}

	protected function prepareTotalCount()
	{
		return $this->_lastResult['total_results'];
	}

	protected function prepareKeys($models)
	{
		$keys = [];

		foreach ($models as &$model) {
			$keys[] = $model['id'];
		}

		return $keys;
	}

}
