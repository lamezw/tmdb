<?php
/* @var $this yii\web\View */

use \yii\helpers\Html;
use \yii\widgets\ActiveForm;

$this->title					 = "Movie: {$movie->title} ({$movie->id})";
$this->params['breadcrumbs'][]	 = $this->title;
?>
<div class="site-movie">
    <h1>
		<?= Html::encode($this->title) ?>
		<?php
		echo Html::a('Delete', ['site/movie', 'id' => $movie->id], [
			'class'	 => 'btn btn-danger',
			'data'	 => [
				'confirm'	 => 'Are you sure you want to delete the movie?',
				'method'	 => 'DELETE',
			]
		]);
		?>
	</h1>

	<?php
	$form							 = ActiveForm::begin([
			'id'			 => 'login-form',
			'options'		 => ['class' => 'form-horizontal'],
			'fieldConfig'	 => [
				'template'		 => "{label}\n<div class=\"col-lg-6\">{input}</div>\n<div class=\"col-lg-4\">{error}</div>",
				'labelOptions'	 => ['class' => 'col-lg-2 control-label'],
			],
	]);
	?>

	<div class="form-group movie-poster">
		<label class="col-lg-2 control-label">Poster</label>
		<div class="col-lg-6">
			<?php
			if ($movie->poster_path) {
				echo '<img src="' . \Yii::getAlias('@web') . $movie->poster_path . '" />';
			}
			?>
		</div>
	</div>

	<?= $form->field($movie, 'title') ?>
	<?= $form->field($movie, 'genres') ?>
	<?= $form->field($movie, 'rating') ?>
	<?= $form->field($movie, 'original_title') ?>
	<?= $form->field($movie, 'release_date') ?>
	<?= $form->field($movie, 'runtime') ?>
	<?= $form->field($movie, 'overview')->textarea(['rows' => '20']) ?>

	<div class="form-group">
		<div class="col-lg-offset-1 col-lg-11">
			<?= Html::submitButton('Save', ['class' => 'btn btn-primary']) ?>
		</div>
	</div>

	<?php ActiveForm::end(); ?>
</div>
