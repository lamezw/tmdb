<?php
/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\grid\ActionColumn;

$this->title					 = 'Movies';
$this->params['breadcrumbs'][]	 = $this->title;
?>
<div class="site-movies">
    <h1>
		<?= Html::encode($this->title) ?>
		<?= Html::a('Popular', ['site/movies']) ?>
		<?= Html::a('On Screen', ['site/movies', 'type' => 'screen']) ?>
	</h1>

	<?php
	echo GridView::widget([
		'dataProvider'	 => $moviesProvider,
		'columns'		 => [
			'id',
			'title',
			'release_date',
			[
				'class'		 => ActionColumn::className(),
				'template'	 => '{view}',
				'buttons'	 => [
					'view' => function ($url, $model) {
						return Html::a('<span class="glyphicon glyphicon-eye-open"></span>', ['site/movie', 'id' => $model->id], [
								'title' => Yii::t('app', 'View'),
						]);
					}
					],
				],
			],
		]);
		?>
</div>
