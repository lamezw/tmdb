<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\filters\VerbFilter;
use app\models\LoginForm;

class SiteController extends Controller
{

	public $defaultAction = 'movies';

	public function behaviors()
	{
		return [
			'access' => [
				'class'	 => AccessControl::className(),
				'only'	 => ['logout', 'movies'],
				'rules'	 => [
					[
						'actions'	 => ['logout', 'movies'],
						'allow'		 => true,
						'roles'		 => ['@'],
					],
				],
			],
			'verbs'	 => [
				'class'		 => VerbFilter::className(),
				'actions'	 => [
					'logout' => ['post'],
				],
			],
		];
	}

	public function actions()
	{
		return [
			'error' => [
				'class' => 'yii\web\ErrorAction',
			],
		];
	}

	public function actionLogin()
	{
		if (!\Yii::$app->user->isGuest) {
			return $this->goHome();
		}

		$model = new LoginForm();
		if ($model->load(Yii::$app->request->post()) && $model->login()) {
			return $this->redirect(['movies']);
		}

		return $this->render('login', [
				'model' => $model,
		]);
	}

	public function actionLogout()
	{
		Yii::$app->user->logout();

		return $this->goHome();
	}

	public function actionMovies($type = null)
	{
		$TMDBService		 = new \app\helpers\TMDBService(\Yii::$app->user->id, \Yii::$app->user->identity->sessionId);
		$TMDBMoviesProvider	 = new \app\helpers\TMDBMoviesProvider($TMDBService, $type);

		return $this->render('movies', [
				'moviesProvider' => $TMDBMoviesProvider,
		]);
	}

	public function actionMovie($id)
	{
		$TMDBService = new \app\helpers\TMDBService(\Yii::$app->user->id, \Yii::$app->user->identity->sessionId);

		if (($movie = \app\models\Movie::findOne($id, $TMDBService)) === null) {
			return $this->redirect(['movies']);
		}

		if (Yii::$app->getRequest()->isDelete) {
			$movie->delete();

			return $this->redirect(['movies']);
		} elseif (Yii::$app->getRequest()->isPost) {
			if ($movie->load(Yii::$app->getRequest()->post())) {
				$movie->save();
			}

			if (!$movie->hasErrors()) {
				return $this->refresh();
			}
		}

		return $this->render('movie', [
				'movie' => $movie,
		]);
	}

}
